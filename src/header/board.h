#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED

#include <cstdlib>

class Board
{
        unsigned int xSize;
        unsigned int ySize;
        std::size_t numRows;
        std::size_t numCols;

        char *grid;

        void initGrid( std::size_t, std::size_t );

        inline size_t index( std::size_t , std::size_t ) const;

    public:

        Board();
        Board( std::size_t, std::size_t );

        void setPos( char, unsigned int, unsigned int );
        char getPos( unsigned int, unsigned int ) const;

        unsigned int getXsize() const { return xSize; }
        unsigned int getYsize() const { return ySize; }
        unsigned int getBoardSize() const { return xSize * ySize; }

        void print();

        ~Board() {delete[] grid;}
};



#endif // BOARD_H_INCLUDED
