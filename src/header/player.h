#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include <cstddef>

struct Piece
{
    int playerID;
    char token;
    unsigned int x;
    unsigned int y;
    bool alive;

    Piece( int id = 0, char t = '\0' ): playerID(id), token(t), x(0), y(0), alive(true){}
};



class Player
{
        char token;
        Piece duke;
        Piece *pawns;
        unsigned int numPawns;

        void initPawns( int );

    public:

        Player( int, char, char );
        Player( int, unsigned int, char, char );

        void initPawnsPos(unsigned int);

        void setPawnPos( unsigned int, unsigned int , unsigned int );
        void getPawnPos( unsigned int, unsigned int&, unsigned int& ) const;
        unsigned int getNumPawns() { return numPawns; }
        char getPawnTok() const {return token;}
        void killPawn ( unsigned int id ) { pawns[id-1].x = 0; pawns[id-1].y = 0; pawns[id-1].alive = false; }
        bool isPawnAlive ( unsigned int id) { return pawns[id-1].alive; }

        void setDukePos( unsigned int, unsigned int );
        void getDukePos( unsigned int&, unsigned int& ) const;
        char getDukeTok() {return duke.token;}
        void killDuke() { duke.x = 0; duke.y = 0; duke.alive = false; }
        bool isDukeAlive() { return duke.alive; }

        bool didLose() const;

        int findPieceAt( unsigned int, unsigned int ) const;
        void movePieceAtTo( unsigned int, unsigned int, unsigned int, unsigned int );


        ~Player() {delete[] pawns;}

        void printPieces();

};




#endif // PLAYER_H_INCLUDED
