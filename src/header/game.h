#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED

#include "board.h"
#include "player.h"

class Game
{
        Board board;
        Player p1;
        Player p2;

        void init();

        void turn( int );
        bool checkPlayerInput( int, unsigned int, unsigned int );
        bool checkDestCoord( int, unsigned int, unsigned int, unsigned int, unsigned int );

        bool blocked( unsigned int, unsigned int, unsigned int, unsigned int ) const;
        bool isPieceAt( unsigned int, unsigned int ) const;
        bool isOppPieceInvolved( int, unsigned int, unsigned int );

        void movePiece( int, unsigned int, unsigned int, unsigned int, unsigned int );

        void checkCap( int, unsigned int, unsigned int );

        void checkOppPawnsCorner( int );
        void checkOppPawnsFlank( int, unsigned int, unsigned int );
        void checkOppPawnsImm( int );
        void checkOppDukeCorner( int );
        void checkOppDukeImm( int );

        void checkMyPawnsCorner( int );
        void checkMyPawnsImm( int );
        void checkMyDukeCorner( int );
        void checkMyDukeImm( int );

        void capture( int, unsigned int, unsigned int, unsigned int );

        bool checkVictory();

    public:

        Game();
        Game( unsigned int xSize, unsigned int ySize, char p1p, char p1d, char p2p, char p2d );

        void run();
};

#endif // GAME_H_INCLUDED
