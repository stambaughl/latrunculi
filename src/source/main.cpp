#include "game.h"

#include <iostream>
#include <iomanip>
#include <string>

using namespace std;

int main()
{
    Game g;

    g.run();

    return 0;
}


