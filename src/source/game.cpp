#include "game.h"

#include "menu.h"

#include <string>
#include <iostream>
#include <iomanip>
#include <conio.h>

using namespace std;

const string a2z = "abcdefghijklmnopqrstuvwxyz";
const string nums = "123456789";

Game::Game(): board(), p1( 1, 'x','X'), p2( 2, 'o','O')
{ init(); }

Game::Game( unsigned int xSize, unsigned int ySize, char p1p = 'x', char p1d = 'X', char p2p = 'o' , char p2d = 'O' )
:board( xSize, ySize), p1( 1, p1p, p1d), p2( 2, p2p, p2d)
{ init(); }

void Game::init()
{
    unsigned int ys = board.getYsize();

    p1.initPawnsPos( 1 ); // initializes p1 pawns position
    p2.initPawnsPos( ys ); // initializes p2 pawns position

    unsigned int xSize = board.getXsize();
    char c1 = p1.getPawnTok(), c2 = p2.getPawnTok();

    for( unsigned int x=1; x<=xSize; ++x)
    {
        board.setPos( c1, x, 1 );
        board.setPos( c2, x, ys );
    }
    unsigned int xs = xSize / 2;

    c1 = p1.getDukeTok(), c2 = p2.getDukeTok();

    if( xSize % 2 == 0) //x size is even
    {
        p1.setDukePos( xs, 2 );
        board.setPos( c1, xs, 2 );

        p2.setDukePos( xs+1, ys-1 );
        board.setPos( c2, xs+1, ys-1 );
    }
    else // x size is odd
    {
        p1.setDukePos( xs+1, 2 );
        board.setPos( c1, xs+1, 2 );

        p2.setDukePos( xs+1, ys-1 );
        board.setPos( c2, xs+1, ys-1 );
    }
}



void Game::run()
{
    for(;;)
    {
        turn( 1 );
        if ( checkVictory() )
            break;

        turn( 2 );
        if ( checkVictory() )
            break;
    }
}

bool Game::checkPlayerInput( int p1p2, unsigned int x0, unsigned int y0i ) //checks input coord, ret true if good input
{
    if( y0i == string::npos +1 )
    {
        cout << "Invalid character!";
        getch();
        return false;
    }
    if( x0 > board.getXsize() || x0 < 1 || y0i > board.getYsize() )
    {
        cout << "Out of bounds! ";
        getch();
        return false;
    }
    if( isPieceAt( x0, y0i ) == false )
    {
        cout << "There is no piece at this position!";
        getch();
        return false;
    }

    if( p1p2 == 1 )
    {
        if( p2.findPieceAt( x0, y0i ) != -1 )
        {
            cout << "This is your opponent's piece!";
            getch();
            return false;
        }
    }

    if( p1p2 == 2 )
    {
        if( p1.findPieceAt( x0, y0i ) != -1 )
        {
            cout << "This is your opponent's piece!";
            getch();
            return false;
        }
    }

    return true;
}

bool Game::checkDestCoord( int p1p2, unsigned int x0, unsigned int y0i, unsigned int xD, unsigned int yDi )
{
    if( yDi == string::npos + 1 )
    {
        cout << "Invalid character!";
        getch();
        return false;
    }
    if( xD > board.getXsize() || xD < 1 || yDi > board.getYsize() )
    {
        cout << "Out of bounds! ";
        getch();
        return false;
    }
    if( isPieceAt( xD, yDi ) )
    {
        cout << "There is already a piece at this position!";
        getch();
        return false;
    }
    if( x0 != xD && y0i != yDi )
    {
        cout << "Illegal move!";
        getch();
        return false;
    }
    if ( blocked( x0, y0i, xD, yDi ) )
    {
        cout << "Blocked!";
        getch();
        return false;
    }

    return true;
}

void Game::turn( int p1p2 )
{
    char choice = '\0', choice2 = '\0';
    unsigned int x0, xD;
    char y0c, yDc;
    unsigned int y0i, yDi;
    string temp;

    bool flag1 = true, flag2 = true, flag3 = true;

    while( flag1 )
    {
        while( flag2 )
        {
            while( flag3 )
            {
                clearScreen();
                board.print();

                cout << "\n\n\n\n";
                cout <<  "Player " << p1p2 << ":\n"
                     << "Enter the coordinates of the piece to move: ";
                getline( cin, temp );
                y0c = temp[ temp.find_first_of( a2z ) ];
                x0 = temp [ temp.find_first_of( nums ) ] - '0' ;
                y0i = a2z.find_first_of(y0c) + 1;

                if( checkPlayerInput( p1p2, x0, y0i) )
                    break;
            }

            if( !flag3 )
            {

                clearScreen();
                board.print();
                cout << "\n\n\n\n";
                cout <<  "Player " << p1p2 << ":\n";
            }

            cout << "Moving piece at (" << y0c << "," << x0 << ").\n"
                 << "Press 'C' to cancel, any other character to continue: ";
            choice = getch();

            if( choice == 'c' || choice == 'C' )
                flag3 = true;
            else
                break;
        }

        cout << "\nEnter the destination coordinates: ";
        getline( cin, temp );
        yDc = temp[ temp.find_first_of( a2z ) ];
        xD = temp [ temp.find_first_of( nums ) ] - '0' ;
        yDi = a2z.find_first_of(yDc) + 1;

        if( checkDestCoord( p1p2, x0, y0i, xD, yDi ) )
        {
            cout << "Destination: (" << yDc << "," << xD << "). Press 'C' to cancel,"
                 << " any other key to finalize move: ";
            choice2 = getch();

            if( choice2 == 'c' || choice2 == 'C' )
            {
                flag2 = true;
                flag3 = false;
            }
            else
                flag1 = false;
        }
        else
        {
            flag2 = true;
            flag3 = false;
        }
    }

    movePiece( p1p2, x0, y0i, xD, yDi );
    checkCap( p1p2, xD, yDi );
}

bool Game::checkVictory()
{
    if( p1.didLose() )
    {
        clearScreen();
        cout << "PLAYER TWO WINS!!!!";
        return true;
    }
    if( p2.didLose() )
    {
        clearScreen();
        cout << "PLAYER ONE WINS!!!!";
        return true;
    }
    return false;
}


void Game::movePiece( int p1p2, unsigned int x0, unsigned int y0, unsigned int xD, unsigned int yD )
{
    board.setPos( ' ', x0, y0 );

    if( p1p2 == 1 )
    {
        if( p1.findPieceAt( x0, y0 ) == 0 )
            board.setPos( p1.getDukeTok(), xD, yD );
        else if( p1.findPieceAt( x0, y0 ) > 0 )
            board.setPos( p1.getPawnTok(), xD, yD );

        p1.movePieceAtTo( x0, y0, xD, yD );
    }
    else if( p1p2 == 2 )
    {
        if( p2.findPieceAt( x0, y0 ) == 0 )
            board.setPos( p2.getDukeTok(), xD, yD );
        else if( p2.findPieceAt( x0, y0 ) > 0 )
            board.setPos( p2.getPawnTok(), xD, yD );

        p2.movePieceAtTo( x0, y0, xD, yD );
    }
}

bool Game::blocked( unsigned int x0, unsigned int y0, unsigned int xD, unsigned int yD ) const
{
    if( y0 == yD )
    {
        if( x0 < xD )
        {
            unsigned int x = x0+1;
            while( x < xD )
            {
                if ( isPieceAt( x, y0 ) == true )
                    return true;
                ++x;
            }
        }
        else
        {
            unsigned int x = xD+1;
            while( x < x0 )
            {
                if ( isPieceAt( x, y0 ) == true )
                    return true;
                ++x;
            }
        }
    }

    else if( x0 == xD )
    {
        if( y0 < yD )
        {
            unsigned int y = y0 +1;
            while( y < yD )
            {
                if ( isPieceAt( x0, y ) == true )
                    return true;
                ++y;
            }
        }
        else
        {
            unsigned int y = yD +1;
            while( y < y0 )
            {
                if ( isPieceAt( x0, y ) == true )
                    return true;
                ++y;
            }
        }
    }

    return false;
}

bool Game::isPieceAt( unsigned int x, unsigned int y ) const
{
    if( p1.findPieceAt( x, y ) == -1 && p2.findPieceAt( x, y ) == -1 )
        return false;
    else
        return true;
}

void Game::checkCap( int p1p2, unsigned int xD, unsigned int yD )
{
    checkOppPawnsFlank( p1p2, xD, yD );
    checkOppPawnsCorner( p1p2 );
    checkOppPawnsImm( p1p2 );
    checkOppDukeCorner( p1p2 );
    checkOppDukeImm( p1p2 );

    // i cannot flank myself, no need to check
    checkMyPawnsCorner( p1p2 );
    checkMyPawnsImm( p1p2 );
    checkMyDukeCorner( p1p2 );
    checkMyDukeImm( p1p2 );
}

void Game::capture( int p1p2, unsigned int id, unsigned int x, unsigned int y )
{
    board.setPos( ' ', x, y );

    if( p1p2 == 1)
    {
        if ( id == 0 )
            p1.killDuke();
        else if ( id > 0 && id < p1.getNumPawns() + 1 )
            p1.killPawn( id );
    }
    else if( p1p2 == 2 )
    {
        if ( id == 0 )
            p2.killDuke();
        else if ( id > 0 && id < p2.getNumPawns() + 1 )
            p2.killPawn( id );
    }
}

bool didJustMove( unsigned int x, unsigned int y, unsigned int xD, unsigned int yD )
{
    if( x == xD && y == yD )
        return true;
    else
        return false;
}

void Game::checkOppPawnsCorner( int p1p2 )
{
    unsigned int numPawnsP1 = p1.getNumPawns();
    unsigned int numPawnsP2 = p2.getNumPawns();
    unsigned int xSize = board.getXsize();
    unsigned int ySize = board.getYsize();
    unsigned int x, y;

    if( p1p2 == 1 ) //check if p2 pawns in corner, p1 must have completed the cornering b/c he just moved
    {
        for( unsigned int id=1; id <= numPawnsP2; ++id )
        {
            if( p2.isPawnAlive( id ) )
            {
                p2.getPawnPos( id, x, y );

                if(  y == 1 &&  x == 1 ) //pawn is in top left corner
                {
                    if( isPieceAt( 2, 1 ) && isPieceAt( 1, 2 ) )
                        capture( 2, id, x, y ); //capture p2 pawn
                }

                if( y == 1 && x == xSize ) //top right corner
                {
                    if ( isPieceAt( xSize-1, 1) && isPieceAt( xSize, 2 ) )
                        capture( 2, id, x, y );
                }

                if( y == ySize && x == 1 ) //bottom left corner
                {
                    if( isPieceAt( 1, ySize-1 ) && isPieceAt( 2, ySize ) )
                        capture( 2, id, x, y );
                }

                if( y == ySize && x == xSize ) //bottom right corner
                {
                    if( isPieceAt( xSize-1, ySize ) && isPieceAt( xSize, ySize-1 ) )
                        capture( 2, id, x, y );
                }
            }
        }
    }
    else if( p1p2 == 2 ) // check p1 pawns in corner, p2 must have completed the cornering b/c he just moved
    {
        for( unsigned int id=1; id <= numPawnsP1; ++id )
        {
            if( p1.isPawnAlive( id ) )
            {
                p1.getPawnPos( id, x, y );

                if(  y == 1 &&  x == 1 ) //pawn is in top left corner
                {
                    if( isPieceAt( 2, 1 ) && isPieceAt( 1, 2 ) )
                        capture( 1, id, x,y );
                }

                if( y == 1 && x == xSize ) //top right corner
                {
                    if ( isPieceAt( xSize-1, 1) && isPieceAt( xSize, 2 ) )
                        capture( 1, id, x,y );
                }

                if( y == ySize && x == 1 ) //bottom left corner
                {
                    if( isPieceAt( 1, ySize-1 ) && isPieceAt( 2, ySize ) )
                        capture( 1, id, x,y );
                }

                if( y == ySize && x == xSize ) //bottom right corner
                {
                    if( isPieceAt( xSize-1, ySize ) && isPieceAt( xSize, ySize-1 ) )
                        capture( 1, id, x,y );
                }
            }
        }
    }
}

void Game::checkOppPawnsFlank( int p1p2, unsigned int xD, unsigned int yD )
{
    unsigned int numPawnsP1 = p1.getNumPawns();
    unsigned int numPawnsP2 = p2.getNumPawns();
    unsigned int x, y;

    if ( p1p2 == 1 ) //check p2 pawns for flanking
    {
        for( unsigned int id=1; id <= numPawnsP2; ++id )
        {
            if ( p2.isPawnAlive( id ) )
            {
                p2.getPawnPos( id, x, y );

                if( p1.findPieceAt( x-1, y ) != -1 && p1.findPieceAt( x+1, y ) != -1 &&
                  ( didJustMove( x-1, y, xD, yD ) || didJustMove( x+1, y,  xD, yD) ) )
                    capture( 2, id, x, y );
                if( p1.findPieceAt( x, y-1 ) != -1 && p1.findPieceAt( x, y+1 ) != -1 &&
                  ( didJustMove( x, y-1, xD, yD ) || didJustMove( x, y+1,  xD, yD) ) )
                    capture( 2, id, x, y );
            }
        }
    }

    else if ( p1p2 == 2 ) //check p1 pawns for flanking
    {
        for( unsigned int id=1; id <= numPawnsP1; ++id )
        {
            if ( p1.isPawnAlive( id ) )
            {
                p1.getPawnPos( id, x, y );

                if( p2.findPieceAt( x-1, y ) != -1 && p2.findPieceAt( x+1, y ) != -1 &&
                  ( didJustMove( x-1, y, xD, yD ) || didJustMove( x+1, y,  xD, yD) ) )
                    capture( 1, id, x, y );
                if( p2.findPieceAt( x, y-1 ) != -1 && p2.findPieceAt( x, y+1 ) != -1 &&
                  ( didJustMove( x, y-1, xD, yD ) || didJustMove( x, y+1,  xD, yD) ) )
                    capture( 1, id, x, y );
            }
        }
    }
}

bool Game::isOppPieceInvolved( int opp, unsigned int x, unsigned int y )
{
    if( opp == 2 )
    {
        if( p2.findPieceAt( x+1, y ) != -1 || p2.findPieceAt( x-1, y ) != -1 ||
            p2.findPieceAt( x, y+1 ) != -1 || p2.findPieceAt( x, y-1 ) != -1 )
            return true;
        else
            return false;
    }

    else if( opp == 1 )
    {
        if( p1.findPieceAt( x+1, y ) != -1 || p1.findPieceAt( x-1, y ) != -1 ||
            p1.findPieceAt( x, y+1 ) != -1 || p1.findPieceAt( x, y-1 ) != -1 )
            return true;
        else
            return false;
    }
    else
        return false;
}

void Game::checkOppPawnsImm( int p1p2 )
{
    unsigned int numPawnsP1 = p1.getNumPawns();
    unsigned int numPawnsP2 = p2.getNumPawns();
    unsigned int xSize = board.getXsize();
    unsigned int ySize = board.getYsize();
    unsigned int x, y;

    if ( p1p2 == 1 )
    {
        for( unsigned int id=1; id <= numPawnsP2; ++id )
        {
            if( p2.isPawnAlive( id ) )
            {
                p2.getPawnPos( id, x ,y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p2 pawn is surrounded on all sides
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x ,y ) ) // and p1 piece is involved
                        capture( 2, id, x, y );

                if( y == 1 && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&// if p2 is against top edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) ) // and p1 involved
                        capture( 2, id, x, y );

                if( isPieceAt( x, y-1 ) && y == ySize && isPieceAt( x+1, y ) && // if p2 pawn is against bottom edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) ) // and p1 involved
                        capture( 2, id, x, y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && x == xSize && //if p2 pawn is against right edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) ) // and p1 involved
                        capture( 2, id, x, y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if  against left edge
                    x==1 && isOppPieceInvolved( 1, x, y ) ) // and p1 involved
                        capture( 2, id, x, y );
            }
        }
    }

    else if ( p1p2 == 2 )
    {
        for( unsigned int id=1; id <= numPawnsP1; ++id )
        {
            p1.getPawnPos( id, x, y );

            if( p1.isPawnAlive( id ) )
            {
                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p1 pawn is surrounded on all sides
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x ,y ) ) //and p2 piece is involved
                        capture( 1, id, x,y );

                if( y == 1 && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p1 pawn is against top edge
                     isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x ,y ) ) //and p1 piece involved
                        capture( 1, id, x,y );

                if( isPieceAt( x, y-1 ) && y == ySize && isPieceAt( x+1, y ) && // if against bottom edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x ,y ) ) // and p1...
                        capture( 1, id, x,y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && x == xSize &&  //if against right edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x ,y ) ) //and p1...
                        capture( 1, id, x,y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if against left edge
                    x==1 && isOppPieceInvolved( 2, x ,y ) )
                        capture( 1, id, x,y );
            }
        }
    }
}

void Game::checkOppDukeCorner( int p1p2 )
{
    unsigned int xSize = board.getXsize();
    unsigned int ySize = board.getYsize();
    unsigned int x, y;

    if( p1p2 == 1 ) //we know player 1 did the cornering because it's his turn and a piece cannot corner itself
    {
        if( p2.isDukeAlive() )
        {
            p2.getDukePos( x, y );

            if(  y == 1 &&  x == 1 ) //oppenents duke is in top left corner
            {
                if( isPieceAt( 2, 1 ) && isPieceAt( 1, 2 ) )
                    capture( 2, 0, x, y ); //capture duke
            }

            if( y == 1 && x == xSize ) //top right corner
            {
                if ( isPieceAt( xSize-1, 1) && isPieceAt( xSize, 2 ) )
                    capture( 2, 0, x, y );
            }

            if( y == ySize && x == 1 ) //bottom left corner
            {
                if( isPieceAt( 1, ySize-1 ) && isPieceAt( 2, ySize ) )
                    capture( 2, 0, x, y );
            }

            if( y == ySize && x == xSize ) //bottom right corner
            {
                if( isPieceAt( xSize-1, ySize ) && isPieceAt( xSize, ySize-1 ) )
                    capture( 2, 0, x, y );
            }
        }
    }

    else if( p1p2 == 2 ) // we know p2 did the cornering for the same reason as player 1
    {
        if( p1.isDukeAlive() )
        {
            p1.getDukePos( x, y );

            if(  y == 1 &&  x == 1 ) //oppenents duke is in top left corner
            {
                if( isPieceAt( 2, 1 ) && isPieceAt( 1, 2 ) )
                    capture( 2, 0, x, y ); //capture duke
            }

            if( y == 1 && x == xSize ) //top right corner
            {
                if ( isPieceAt( xSize-1, 1) && isPieceAt( xSize, 2 ) )
                    capture( 2, 0, x, y );
            }

            if( y == ySize && x == 1 ) //bottom left corner
            {
                if( isPieceAt( 1, ySize-1 ) && isPieceAt( 2, ySize ) )
                    capture( 2, 0, x, y );
            }

            if( y == ySize && x == xSize ) //bottom right corner
            {
                if( isPieceAt( xSize-1, ySize ) && isPieceAt( xSize, ySize-1 ) )
                    capture( 2, 0, x, y );
            }
        }
    }
}

void Game::checkOppDukeImm( int p1p2 )
{
    unsigned int x, y;
    unsigned int ySize = board.getYsize();
    unsigned int xSize = board.getXsize();

    if( p1p2 == 1 )
    {
        if( p2.isDukeAlive() )
        {
            p2.getDukePos( x ,y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p2 duke is surrounded on all sides
                isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x ,y ) )
                    capture( 2, 0, x, y );

            if( y == 1 && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&// if against top edge
                isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) )
                    capture( 2, 0, x, y );

            if( isPieceAt( x, y-1 ) && y == ySize && isPieceAt( x+1, y ) && // if against bottom edge
                isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) )
                    capture( 2, 0, x, y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && x == xSize &&
                isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) ) //if against right edge
                    capture( 2, 0, x, y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&
                x==1 && isOppPieceInvolved( 1, x, y ) ) // if against left edge
                    capture( 2, 0, x, y );
        }
    }

    else if( p1p2 == 2 )
    {
        if( p1.isDukeAlive() )
        {
            p1.getDukePos( x ,y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p1 duke is surrounded
                isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x ,y ) )
                    capture( 1, 0, x, y );

            if( y == 1 && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if against top edge
                isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) )
                    capture( 1, 0, x, y );

            if( isPieceAt( x, y-1 ) && y == ySize && isPieceAt( x+1, y ) && // if  against bottom edge
                isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) )
                    capture( 1, 0, x, y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && x == xSize &&
                isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) ) //if against right edge
                    capture( 1, 0, x, y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&
                x==1 && isOppPieceInvolved( 2, x, y ) ) // if against left edge
                    capture( 1, 0, x, y );
        }
    }
}

void Game::checkMyPawnsCorner( int p1p2 )
{
    unsigned int numPawnsP1 = p1.getNumPawns();
    unsigned int numPawnsP2 = p2.getNumPawns();
    unsigned int xSize = board.getXsize();
    unsigned int ySize = board.getYsize();
    unsigned int x, y;

    if( p1p2 == 1 ) //p1 pawns in corner? maybe p1 cornered his own piece, so must see if opp piece is involved
    {
        for( unsigned int id=1; id <= numPawnsP1; ++id )
        {
            if( p1.isPawnAlive( id ) )
            {
                p1.getPawnPos( id, x, y );

                if(  y == 1 &&  x == 1 ) //pawn is in top left corner
                {
                    if( isPieceAt( 2, 1 ) && isPieceAt( 1, 2 ) &&
                      ( isOppPieceInvolved( 2, 2, 1 )  || isOppPieceInvolved( 2, 1, 2 )  ) )
                        capture( 1, id, x, y ); //capture p2 pawn
                }

                if( y == 1 && x == xSize ) //top right corner
                {
                    if ( isPieceAt( xSize-1, 1) && isPieceAt( xSize, 2 ) &&
                       ( isOppPieceInvolved( 2, xSize-1, 1 ) || isOppPieceInvolved( 2, xSize, 2 ) ) )
                        capture( 1, id, x, y );
                }

                if( y == ySize && x == 1 ) //bottom left corner
                {
                    if( isPieceAt( 1, ySize-1 ) && isPieceAt( 2, ySize ) &&
                      ( isOppPieceInvolved( 2, 1, ySize-1 ) || isOppPieceInvolved( 2, 2, ySize )  ) )
                        capture( 1, id, x, y );
                }

                if( y == ySize && x == xSize ) //bottom right corner
                {
                    if( isPieceAt( xSize-1, ySize ) && isPieceAt( xSize, ySize-1 ) &&
                      ( isOppPieceInvolved( 2, xSize-1, ySize ) || isOppPieceInvolved( 2, xSize, ySize - 1 )  ) )
                        capture( 1, id, x, y );
                }
            }
        }
    }
    else if( p1p2 == 2 ) // p2 pawns in corner? maybe p2 cornered his own piece, so must see if opp piece is involved
    {
        for( unsigned int id=1; id <= numPawnsP2; ++id )
        {
            if( p2.isPawnAlive( id ) )
            {
                p2.getPawnPos( id, x, y );

                if(  y == 1 &&  x == 1 ) //pawn is in top left corner
                {
                    if( isPieceAt( 2, 1 ) && isPieceAt( 1, 2 ) &&
                      ( isOppPieceInvolved( 1, 2, 1 )  || isOppPieceInvolved( 1, 1, 2 )  ) )
                        capture( 2, id, x, y ); //capture p2 pawn
                }

                if( y == 1 && x == xSize ) //top right corner
                {
                    if ( isPieceAt( xSize-1, 1) && isPieceAt( xSize, 2 ) &&
                       ( isOppPieceInvolved( 1, xSize-1, 1 ) || isOppPieceInvolved( 1, xSize, 2 ) ) )
                        capture( 2, id, x, y );
                }

                if( y == ySize && x == 1 ) //bottom left corner
                {
                    if( isPieceAt( 1, ySize-1 ) && isPieceAt( 2, ySize ) &&
                      ( isOppPieceInvolved( 1, 1, ySize-1 ) || isOppPieceInvolved( 1, 2, ySize )  ) )
                        capture( 2, id, x, y );
                }

                if( y == ySize && x == xSize ) //bottom right corner
                {
                    if( isPieceAt( xSize-1, ySize ) && isPieceAt( xSize, ySize-1 ) &&
                      ( isOppPieceInvolved( 1, xSize-1, ySize ) || isOppPieceInvolved( 1, xSize, ySize - 1 )  ) )
                        capture( 2, id, x, y );
                }
            }
        }
    }
}

void Game::checkMyPawnsImm( int p1p2 )
{
    unsigned int numPawnsP1 = p1.getNumPawns();
    unsigned int numPawnsP2 = p2.getNumPawns();
    unsigned int xSize = board.getXsize();
    unsigned int ySize = board.getYsize();
    unsigned int x, y;

    if ( p1p2 == 1 )
    {
        for( unsigned int id=1; id <= numPawnsP2; ++id )
        {
            if( p1.isPawnAlive( id ) )
            {
                p1.getPawnPos( id, x ,y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p1 pawn is surrounded on all sides
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x ,y ) ) // and p2 piece is involved
                        capture( 1, id, x, y );

                if( y == 1 && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&// if p1 is against top edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) ) // and p2 involved
                        capture( 1, id, x, y );

                if( isPieceAt( x, y-1 ) && y == ySize && isPieceAt( x+1, y ) && // if p1 pawn is against bottom edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) ) // and p2 involved
                        capture( 1, id, x, y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && x == xSize && //if p1 pawn is against right edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) ) // and p2 involved
                        capture( 1, id, x, y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if  against left edge
                    x==1 && isOppPieceInvolved( 2, x, y ) ) // and p2 involved
                        capture( 1, id, x, y );
            }
        }
    }

    if ( p1p2 == 2 )
    {
        for( unsigned int id=1; id <= numPawnsP1; ++id )
        {
            if( p2.isPawnAlive( id ) )
            {
                p2.getPawnPos( id, x ,y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p2 pawn is surrounded on all sides
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x ,y ) ) // and p1 piece is involved
                        capture( 2, id, x, y );

                if( y == 1 && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&// if p2 is against top edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) ) // and p1 involved
                        capture( 2, id, x, y );

                if( isPieceAt( x, y-1 ) && y == ySize && isPieceAt( x+1, y ) && // if p2 pawn is against bottom edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) ) // and p1 involved
                        capture( 2, id, x, y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && x == xSize && //if p2 pawn is against right edge
                    isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) ) // and p1 involved
                        capture( 2, id, x, y );

                if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if  against left edge
                    x==1 && isOppPieceInvolved( 1, x, y ) ) // and p1 involved
                        capture( 2, id, x, y );
            }
        }
    }
}

void Game::checkMyDukeCorner( int p1p2 )
{
    unsigned int xSize = board.getXsize();
    unsigned int ySize = board.getYsize();
    unsigned int x, y;

    if( p1p2 == 1 ) //we know player 1 did the cornering because it's his turn and piece cannot corner itself
    {
        if( p1.isDukeAlive() )
        {
            p1.getDukePos( x, y );

            if(  y == 1 &&  x == 1 ) //oppenents duke is in top left corner
            {
                if( isPieceAt( 2, 1 ) && isPieceAt( 1, 2 ) && isOppPieceInvolved( 2, x, y ) )
                    capture( 1, 0, x, y ); //capture duke
            }

            if( y == 1 && x == xSize ) //top right corner
            {
                if ( isPieceAt( xSize-1, 1) && isPieceAt( xSize, 2 ) && isOppPieceInvolved( 2, x, y ) )
                    capture( 1, 0, x, y );
            }

            if( y == ySize && x == 1 ) //bottom left corner
            {
                if( isPieceAt( 1, ySize-1 ) && isPieceAt( 2, ySize ) && isOppPieceInvolved( 2, x, y ) )
                    capture( 1, 0, x, y );
            }

            if( y == ySize && x == xSize ) //bottom right corner
            {
                if( isPieceAt( xSize-1, ySize ) && isPieceAt( xSize, ySize-1 ) && isOppPieceInvolved( 2, x, y ) )
                    capture( 1, 0, x, y );
            }
        }
    }

    else if( p1p2 == 2 )
    {
        if( p2.isDukeAlive() )
        {
            p2.getDukePos( x, y );

            if(  y == 1 &&  x == 1 ) //oppenents duke is in top left corner
            {
                if( isPieceAt( 2, 1 ) && isPieceAt( 1, 2 ) && isOppPieceInvolved( 1, x, y ) )
                    capture( 2, 0, x, y ); //capture duke
            }

            if( y == 1 && x == xSize ) //top right corner
            {
                if ( isPieceAt( xSize-1, 1) && isPieceAt( xSize, 2 ) && isOppPieceInvolved( 1, x, y ) )
                    capture( 2, 0, x, y );
            }

            if( y == ySize && x == 1 ) //bottom left corner
            {
                if( isPieceAt( 1, ySize-1 ) && isPieceAt( 2, ySize ) && isOppPieceInvolved( 1, x, y ) )
                    capture( 2, 0, x, y );
            }

            if( y == ySize && x == xSize ) //bottom right corner
            {
                if( isPieceAt( xSize-1, ySize ) && isPieceAt( xSize, ySize-1 ) && isOppPieceInvolved( 1, x, y ) )
                    capture( 2, 0, x, y );
            }
        }
    }
}

void Game::checkMyDukeImm( int p1p2 )
{
    unsigned int x, y;
    unsigned int ySize = board.getYsize();
    unsigned int xSize = board.getXsize();

    if( p1p2 == 1 )
    {
        if( p1.isDukeAlive() )
        {
            p1.getDukePos( x ,y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p1 duke is surrounded on all sides
                isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x ,y ) )
                    capture( 1, 0, x, y );

            if( y == 1 && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&// if against top edge
                isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) )
                    capture( 1, 0, x, y );

            if( isPieceAt( x, y-1 ) && y == ySize && isPieceAt( x+1, y ) && // if against bottom edge
                isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) )
                    capture( 1, 0, x, y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && x == xSize &&
                isPieceAt( x-1, y ) && isOppPieceInvolved( 2, x, y ) ) //if against right edge
                    capture( 1, 0, x, y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&
                x == 1 && isOppPieceInvolved( 2, x, y ) ) // if against left edge
                    capture( 1, 0, x, y );
        }
    }

    else if( p1p2 == 2 )
    {
        if( p2.isDukeAlive() )
        {
            p2.getDukePos( x ,y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if p2 duke is surrounded
                isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x ,y ) )
                    capture( 2, 0, x, y );

            if( y == 1 && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) && // if against top edge
                isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) )
                    capture( 2, 0, x, y );

            if( isPieceAt( x, y-1 ) && y == ySize && isPieceAt( x+1, y ) && // if  against bottom edge
                isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) )
                    capture( 2, 0, x, y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && x == xSize &&
                isPieceAt( x-1, y ) && isOppPieceInvolved( 1, x, y ) ) //if against right edge
                    capture( 2, 0, x, y );

            if( isPieceAt( x, y-1 ) && isPieceAt( x, y+1 ) && isPieceAt( x+1, y ) &&
                x==1 && isOppPieceInvolved( 1, x, y ) ) // if against left edge
                    capture( 2, 0, x, y );
        }
    }
}
