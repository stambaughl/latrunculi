#include "support.h"

#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

void clearScreen()
{
    cout << string( 100, '\n' );
}

void mainScreen()
{
    clearScreen();

    cout << right << "\n\n\n\n"
         << setw(54) << "*************************\n" //width of 25 characters
         << setw(54) << "*      LATRUNCULI       *\n"
         << setw(54) << "* (Ludus latrunculorum) *\n"
         << setw(54) << "*                       *\n"
         << setw(54) << "*  By: Logan Stambaugh  *\n"
         << setw(54) << "*************************\n" << endl;

    cout << "\n\n\n" << setw(52) << "Enter 'N' for new game\n";
    cout << setw(54) << "Enter 'S' to load saved game" << endl;
    cout << "\n\n\n\n\n";
}
