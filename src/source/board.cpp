#include "board.h"

#include <string>
#include <iostream>

using namespace std;

const string a2z = "abcdefghijklmnopqrstuvwxyz";

Board::Board(): xSize(8), ySize(8)
{
    initGrid( xSize, ySize );
}

Board::Board( size_t x_size, size_t y_size ): xSize(x_size), ySize(y_size)
{
    initGrid( xSize, ySize );
}

inline size_t Board::index( size_t xi, size_t yi ) const
{
    return  xi + ( numCols * yi );
}

void Board::setPos( char c, unsigned int x, unsigned int y ) //sets the given position to the given char
{
    if( y == 0 || x == 0 )
        return;
    else
        grid[ index( (x*2 + 1) , y ) ] = c;
}



char Board::getPos( unsigned int x, unsigned int y) const
{
        return grid[ index( (x*2 + 1) , y ) ];
}


void Board::initGrid(size_t xSize, size_t ySize)
{
    unsigned int a2zCount = 0;
    const string nums = "123456789";

    numRows = 1 + ySize;
    numCols = 3 + (xSize*2);
    grid = new char[ numCols * numRows ];

    for( unsigned int xi=0; xi<numCols; ++xi ) // initializes the xi-labels
    {
        if( xi<3 )
            grid[ index(xi,0) ] = ' ';
        else
        {
            grid[ index(xi,0) ] = nums[( (xi-1) / 2 ) - 1];
            grid[ index(xi+1, 0) ] = ' ';
            ++xi;
        }
    }

    for( unsigned int yi=1; yi<numRows; ++yi ) //initializes the y-labels
    {
        grid[ index(0,yi) ] = a2z[a2zCount];
        ++a2zCount;
    }


    for( unsigned int yi=1; yi<numRows; ++yi) //initializes the grid, row by row, top to bottom
    {
        for( unsigned int xi=1; xi<numCols; xi+=2 )
        {
                grid[ index(xi, yi) ] = ' ';
                grid[ index(xi+1, yi) ]  = '|';
        }
    }
}

void Board::print()
{
    for( unsigned int yi=0; yi<numRows; ++yi )
    {
        for( unsigned int xi=0; xi<numCols; ++xi )
        {
            cout << grid[ index(xi, yi) ];
        }

        cout << "\n";
    }
}


