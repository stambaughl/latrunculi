 #include "menu.h"

 #include <fstream>
 #include <iostream>
 #include <iomanip>
 #include <string>


using namespace std;

void clearScreen()
{
    system("CLS");
}

void mainBanner()
{
    clearScreen();

    cout << right << "/n/n/n/n"
         << setw(54) << "*************************\n" //width of 25 characters
         << setw(54) << "*      LATRUNCULI       *\n"
         << setw(54) << "* (Ludus latrunculorum) *\n"
         << setw(54) << "*                       *\n"
         << setw(54) << "*  By: Logan Stambaugh  *\n"
         << setw(54) << "*************************\n" << endl;

}

void mainMenu()
{
    mainBanner();

    cout << "\n\n\n" << setw(52) << "Enter 'N' for new game\n";
    cout << "\n\n\n\n\n";
}


char mainChoice(char choice)
{
    for(;;)
    {
        mainMenu();
        cin >> choice;

        if (choice == 'N' || choice == 'n')
            return choice;
    }
}

 void saveMenu()
{
     mainBanner();

     cout << "\n\n\n" << setw(52) << "Enter saved game name";
     cout << "\n\n\n\n\n";
}

void newGameMenu()
{
    mainBanner();

    cout << "\n\n\n" << setw(61) << "Enter 'D' to play with default settings\n";
    cout << setw(56) << "Enter 'C' to customize settings";
    cout << "\n\n\n\n\n";
}

void newGameChoice(char& choice)
{
    for(;;)
    {
        newGameMenu();
        cin >> choice;

        if (choice == 'D' || choice == 'd' || choice == 'C' || choice == 'c')
            break;
    }
}

//void settingsMenu()


//void getSaved( string filename )

