#include "player.h"

#include <cctype>
#include <cstddef>
#include <string>
#include <iostream>

using namespace std;

Player::Player( int id, char p, char d): token(p), duke( id, d ), numPawns(8)
{
    initPawns( id );
}

Player::Player( int id, unsigned int numP, char p, char d ): token(p), duke(d), numPawns(numP)
{
    initPawns( id );
}

void Player::initPawns( int id )
{
    pawns = new Piece[numPawns];

    for(unsigned int i=0; i<numPawns; ++i)
    {
        pawns[i].token = token;
        pawns[i].playerID = id;
    }
}

void Player::initPawnsPos(unsigned int y)
{
    for( unsigned int i=0; i<numPawns; ++i )
    {
        pawns[i].y = y;  pawns[i].x = i+1;
    }
}

// searches for pawn or duke at a certain position, returns 0 if duke, -1 if no match
int Player::findPieceAt( unsigned int x, unsigned int y) const
{
    if( duke.x == x && duke.y == y )
        return 0;

    for(unsigned int i=0; i<numPawns; ++i)
    {
        if( pawns[i].x == x && pawns[i].y == y )
            return i+1;
    }

    return -1;
}

void Player::movePieceAtTo( unsigned int x0, unsigned int y0, unsigned int xD, unsigned int yD )
{
    int id = findPieceAt( x0, y0 );

    if( id == -1 )
        cout << "No piece found by findPieceAt";
    else if ( id == 0 )
    {
        duke.x = xD;
        duke.y = yD;
    }
    else
    {
        pawns[id-1].x = xD;
        pawns[id-1].y = yD;
    }
}

void Player::setPawnPos( unsigned int id, unsigned int x, unsigned int y ) //sets the position of a pawn, IDs are [1, numPawns]
{
    pawns[id-1].x = x;    pawns[id-1].y = y;
}

void Player::getPawnPos( unsigned int id, unsigned int& x, unsigned int& y ) const
{
    x = pawns[id-1].x;   y = pawns[id-1].y;
}

void Player::printPieces()
{
    for(unsigned int i=0; i<numPawns; ++i)
    {
        cout << i+1 << ") Player ID: " << pawns[i].playerID << " " << pawns[i].token << " " << pawns[i].x << " " << pawns[i].y << " "
             << pawns[i].alive << "\n";
    }

    cout << "DUKE) Player ID: " << duke.playerID << " " << duke.token << " " << duke.x << " " << duke.y << " " << duke.alive << "\n";
}

void Player::setDukePos( unsigned int x, unsigned int y )
{
    duke.x = x;    duke.y = y;
}

void Player::getDukePos( unsigned int& x, unsigned int& y ) const
{
    x = duke.x;   y = duke.y;
}

bool Player::didLose() const
{
    if( !duke.alive )
        return true;

    bool lose = true;

    for( unsigned int i=0; i < numPawns; ++i )
    {
        if( pawns[i].alive )
            lose = false;
    }

    return lose;
}



