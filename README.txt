* ABOUT
	This is a simple app that was originally created as a final project for a 200-level com-sci class.
It currently only runs on Windows.


* NOTE
	The "support" class contained in support.h and support.cpp includes the beginnings of support 
functions for an interactive menu, which was never realized.  It should not be built with the program 
as is because it contains an alternate function def for clearscreen(). Doing so will result in a 
compilation error.


	




